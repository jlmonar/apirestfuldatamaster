@extends('layouts.app')

@section('content')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <div class="row">
                    {!! Form::model(Request::all(), ['route' => 'obtener-consultas', 'method' => 'GET']) !!}
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        {!!Form::label('fecha_inicio_fin', 'Fecha Inicio:', ['class' => 'control-label'])!!}
                        {!!Form::text('fecha_inicio', null, ['class' => 'form-control', 'placeholder' => 'Fecha Inicio...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                        <br>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        {!!Form::label('fecha_inicio_fin', 'Fecha Fin:', ['class' => 'control-label'])!!}
                        {!!Form::text('fecha_fin', null, ['class' => 'form-control', 'placeholder' => 'Fecha Fin...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <br>
                        <button type="submit" class="btn btn-default btn-search-product">Buscar</button>
                    </div>
                    {!!Form::close() !!}
                </div>
            </div>
            <div class="panel panel-default">
                @if(isset($fecha_inicio) && isset($fecha_fin))
                    <div class="panel-heading">Total consultas entre {{$fecha_inicio}} - {{$fecha_fin}}:</div>
                @else
                    <div class="panel-heading">Total consultas:</div>
                @endif


                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(isset($totalConsultas))
                        {{$totalConsultas}}
                    @else
                        {{ count(Auth::user()->consultas) }}
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('input[name="fecha_inicio"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        //timePicker: true,
        //timePicker24Hour: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('input[name="fecha_fin"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        //timePicker: true,
        //timePicker24Hour: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
</script>
@endsection

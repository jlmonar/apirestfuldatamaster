<?php

namespace App\Http\Controllers\DATASET;

use App\Http\Controllers\ApiController;
use App\Model\Consulta;
use App\Model\Dataset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DatasetController extends ApiController
{
    public function __construct()
    {
        //$this->middleware('client.credentials')->only('show');
        $this->middleware('auth:api')->only('show');

        $this->middleware('can:getDatauser,App\Model\Dataset')->only('show');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        //$this->allowedVerifiedUserAction();

        //Obtengo el usuario logueado.
        //$user = auth()->guard('api')->user();
        //$user = Auth::user()

        //$usuario = Dataset::where('NUMERO_RUC', $codigo)->first();
        $dataset = Dataset::where('IDENTIFICACION', $codigo)->first();

        if (isset($dataset)) {
            $user = Auth::user();

            $consulta = new Consulta();
            $consulta->user_id =  $user->id;
            $consulta->dataset_id = $dataset->ID;
            $consulta->fecha_consulta = date('Y-m-d H:i:s');

            $consulta->save();

            return $this->showOne($dataset);
        }


        return $this->errorResponse("No existe ninguna instancia de usuario con la identificación proporcionada.", 404);
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

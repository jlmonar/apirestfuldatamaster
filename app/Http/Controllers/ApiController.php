<?php

namespace App\Http\Controllers;

use App\Traits\APIResponser;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;

class ApiController extends Controller
{
    use APIResponser;

    protected function allowedVerifiedUserAction() {
        if (Gate::denies('verified-user-action')) {
            throw new AuthorizationException('Esta acción no la tienes permitida.');
        }
    }
}

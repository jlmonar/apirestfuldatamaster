<?php

namespace App\Http\Controllers\CONSULTA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ConsultaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function obtenerConsultas(Request $request) {
        if (is_null($request->fecha_inicio) OR is_null($request->fecha_fin)) {
            return redirect('home');
        }

        $initial_date_formatted = date_create_from_format('d/m/Y', $request->fecha_inicio);
        $end_date_formatted = date_create_from_format('d/m/Y', $request->fecha_fin);

        if ($initial_date_formatted == false or $end_date_formatted == false) {
            return view('home');
        }
        $fecha_inicio = $initial_date_formatted->format('Y-m-d');
        $fecha_fin = $end_date_formatted->format('Y-m-d');

        $user = Auth::user();
        $totalConsultas = $user->consultas()->whereBetween('fecha_consulta',array($fecha_inicio, $fecha_fin))->count();

        return view('home')->with([
            'totalConsultas' => $totalConsultas,
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin
        ]);
    }
}

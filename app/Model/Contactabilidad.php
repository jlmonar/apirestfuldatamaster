<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contactabilidad extends Model
{
    protected $table = 'CONTACTABILIDAD';

    protected $primaryKey = 'IDENTIFICACION';
}

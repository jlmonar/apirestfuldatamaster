<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    protected $table = 'CONSULTAS';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public  function user() {
        return $this->belongsTo(User::class);
    }
    
    public function dataset() {
        return $this->belongsTo(Dataset::class, 'dataset_id');
        //return $this->belongsTo(Dataset::class);
    }
}

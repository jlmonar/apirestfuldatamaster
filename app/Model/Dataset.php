<?php

namespace App\Model;

use App\Transformers\DatasetTransformer;
use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    protected $table = 'DATASET_DP';

    protected $primaryKey = 'ID';

    public $transformer = DatasetTransformer::class;

    public function contactabilidad() {
        return $this->belongsTo(Contactabilidad::class, 'IDENTIFICACION', 'IDENTIFICACION');
    }
}

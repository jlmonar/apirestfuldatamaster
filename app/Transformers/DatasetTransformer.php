<?php

namespace App\Transformers;

use App\Model\Dataset;
use League\Fractal\TransformerAbstract;

class DatasetTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Dataset $dataset)
    {
        return [
            //'tipo_documento' => (string) $dataset->TIPO_DOCUMENTO,
            'identificacion' => (string) $dataset->IDENTIFICACION,

            'nombres' => (string) $dataset->NOMBRES,
            'estudios' => (string) $dataset->ESTUDIOS,
            'sexo' => (string) $dataset->SEXO,
            'profesion' => (string) $dataset->PROFESION,
            'estado_civil' => (string) $dataset->ESTADO_CIVIL,

            'fecha_nacimiento' => (string) $dataset->FECHA_NACIMIENTO,
            //'ciudad_nacimiento' => (string) $dataset->CIUDAD_NACIMIENTO,
            'lugar_nacimiento' => (string) ($dataset->LUGNAC_PROVINCIA.'/'.$dataset->LUGNAC_CANTON.'/'.$dataset->LUGNAC_PARROQUIA),

            'domicilio' => (string) $dataset->DOMICILIO,
            'cedula_madre' => (string) $dataset->CEDULA_MADRE,
            'cedula_padre' => (string) $dataset->CEDULA_PADRE,
            'nombre_padre' => (string) $dataset->NOMBRE_PADRE,
            'nombre_madre' => (string) $dataset->NOMBRE_MADRE,
            'cedula_conyuge' => (string) $dataset->CEDULA_CONYUGE,
            'nombre_conyuge' => (string) $dataset->NOMBRE_CONYUGE,

            'situacion_laboral' => (string) $dataset->SITUACION_LABORAL,

            'cod_division_politica' => (string) $dataset->CODDIVPOL,
            'ruc_patrono' => (string) $dataset->RUCEMP,
            'sucursal' => (string) $dataset->CODSUC,
            'tipo_afiliacion' => (string) $dataset->CODTIPEMP,
            'empresa_patrono' => (string) $dataset->NOMEMP,
            'salario' => (string) $dataset->SALARIO,
            'fecha_ingreso' => (string) $dataset->FECINGAFI,
            'cargo' => (string) $dataset->OCUAFI,

            'numero_ruc' => (string) $dataset->NUMERO_RUC,
            'razon_social' => (string) $dataset->RAZON_SOCIAL,
            'estado' => (string) $dataset->ESTADO_CONTRIBUYENTE,
            'clase_contribuyente' => (string) $dataset->CLASE_CONTRIBUYENTE,
            'fecha_inicio_actividades' => (string) $dataset->FECHA_INICIO_ACTIVIDADES,
            'fecha_actualizacion' => (string) $dataset->FECHA_ACTUALIZACION,
            'fecha_suspension_definitiva' => (string) $dataset->FECHA_SUSPENSION_DEFINITIVA,
            'fecha_reinicio_actividades' => (string) $dataset->FECHA_REINICIO_ACTIVIDADES,
            'obligado' => (string) $dataset->OBLIGADO,
            'tipo_contribuyente' => (string) $dataset->TIPO_CONTRIBUYENTE,
            'numero_establecimiento' => (string) $dataset->NUMERO_ESTABLECIMIENTO,
            'direccion_calle' => (string) $dataset->CALLE,
            'direccion_numero' => (string) $dataset->NUMERO,
            'direccion_interseccion' => (string) $dataset->INTERSECCION,
            'estado_establecimiento' => (string) $dataset->ESTADO_ESTABLECIMIENTO,
            'direccion_provincia' => (string) $dataset->DESCRIPCION_PROVINCIA,
            'direccion_canton' => (string) $dataset->DESCRIPCION_CANTON,
            'direccion_parroquia' => (string) $dataset->DESCRIPCION_PARROQUIA,
            'sector' => (string) $dataset->CODIGO_CIIU,
            'actividad_economica' => (string) $dataset->ACTIVIDAD_ECONOMICA,

            'presenta' => (string) $dataset->PRESENTA,

            'tipo' => (string) $dataset->TIPO,
            'ingresos_estimados_actividades_comerciales' => (double) $dataset->INGRESOS_ESTIMADOS_ACTIVIDADES_COMERCIALES,
            'ingresos_estimados_trabajo_permanente' => (double) $dataset->INGRESOS_ESTIMADOS_TRABAJO_PERMAMENTE,
            'ingresos_estimados_actividades_profesionales' => (double) $dataset->INGRESOS_ESTIMADOS_ACTIVIDADES_PROFESIONALES,
            'otros_ingresos_estimados' => (double) $dataset->OTROS_INGRESOS_ESTIMADOS,
            'gastos_estimados' => (double) $dataset->GASTOS_ESTIMADOS,

            'contactos' => [
                'telefono_1' => isset($dataset->contactabilidad) ? (string) $dataset->contactabilidad->TELEFONO1 : (string) $dataset->TELEFONO1,
                'telefono_2' => isset($dataset->contactabilidad) ? (string) $dataset->contactabilidad->TELEFONO2 : (string) $dataset->TELEFONO2,
                'telefono_3' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->TELEFONO3 : (string) $dataset->TELEFONO3,
                'telefono_4' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->TELEFONO4 : (string) $dataset->TELEFONO4,
                'telefono_5' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->TELEFONO5 : (string) $dataset->TELEFONO5,
                'telefono_6' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->TELEFONO6 : (string) $dataset->TELEFONO6,
                'celular_1' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->CELULAR1 : (string) $dataset->CELULAR1,
                'celular_2' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->CELULAR2 : (string) $dataset->CELULAR2,
                'email_1' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->EMAIL1 : (string) $dataset->EMAIL1,
                'email_2' => isset($dataset->contactabilidad) ? (string)  $dataset->contactabilidad->EMAIL2 : (string) $dataset->EMAIL2,
            ]
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            //'tipo_documento' => 'TIPO_DOCUMENTO',
            'identificacion' => 'IDENTIFICACION',

            'nombres' => 'NOMBRES',
            'estudios' => 'ESTUDIOS',
            'sexo' => 'SEXO',
            'profesion' => 'PROFESION',
            'estado_civil' => 'ESTADO_CIVIL',

            'fecha_nacimiento' => 'FECHA_NACIMIENTO',
            //'ciudad_nacimiento' => 'CIUDAD_NACIMIENTO',
            'lugar_nacimiento' => 'LUGNAC_PROVINCIA',

            'domicilio' => 'DOMICILIO',
            'cedula_madre' => 'CEDULA_MADRE',
            'cedula_padre' => 'CEDULA_PADRE',
            'nombre_padre' => 'NOMBRE_PADRE',
            'nombre_madre' => 'NOMBRE_MADRE',
            'cedula_contuge' => 'CEDULA_CONYUGE',
            'nombre_conyuge' => 'NOMBRE_CONYUGE',

            'situacion_laboral' => 'SITUACION_LABORAL',


            'cod_division_politica' => 'CODDIVPOL',
            'ruc_patrono' => 'RUCEMP',
            'sucursal' => 'CODSUC',
            'tipo_afiliacion' => 'CODTIPEMP',
            'empresa_patrono' => 'NOMEMP',
            'salario' => 'SALARIO',
            'fecha_ingreso' => 'FECINGAFI',
            'cargo' => 'OCUAFI',

            'numero_ruc' => 'NUMERO_RUC',
            'razon_social' => 'RAZON_SOCIAL',
            'estado' => 'ESTADO_CONTRIBUYENTE',
            'clase_contribuyente' => 'CLASE_CONTRIBUYENTE',
            'fecha_inicio_actividades' => 'FECHA_INICIO_ACTIVIDADES',
            'fecha_actualizacion' => 'FECHA_ACTUALIZACION',
            'obligado' => (string) 'OBLIGADO',
            'tipo_contribuyente' => 'TIPO_CONTRIBUYENTE',
            'numero_establecimiento' => 'NUMERO_ESTABLECIMIENTO',
            'direccion_calle' => 'CALLE',
            'direccion_numero' => 'NUMERO',
            'direccion_interseccion' => 'INTERSECCION',
            'estado_establecimiento' => 'ESTADO_ESTABLECIMIENTO',
            'direccion_provincia' => 'DESCRIPCION_PROVINCIA',
            'direccion_canton' => 'DESCRIPCION_CANTON',
            'direccion_parroquia' => 'DESCRIPCION_PARROQUIA',
            'sector' => 'CODIGO_CIIU',
            'actividad_economica' => 'ACTIVIDAD_ECONOMICA',

            'presenta' => 'PRESENTA',

            'tipo' => 'TIPO',
            'ingresos_estimados_actividades_comerciales' => 'INGRESOS_ESTIMADOS_ACTIVIDADES_COMERCIALES',
            'ingresos_estimados_trabajo_permanente' => 'INGRESOS_ESTIMADOS_TRABAJO_PERMAMENTE',
            'ingresos_estimados_actividades_profesionales' => 'INGRESOS_ESTIMADOS_ACTIVIDADES_PROFESIONALES',
            'otros_ingresos_estimados' => 'OTROS_INGRESOS_ESTIMADOS',
            'gastos_estimados' => 'GASTOS_ESTIMADOS',
            'telefono_1' => 'TELEFONO1',
            'telefono_2' => 'TELEFONO2',
            'telefono_3' => 'TELEFONO3',
            'telefono_4' => 'TELEFONO4',
            'telefono_5' => 'TELEFONO5',
            'telefono_6' => 'TELEFONO6',
            'celular_1' => 'CELULAR1',
            'celular_2' => 'CELULAR2',
            'email_1' => 'EMAIL1',
            'email_2' => 'EMAIL2',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers\Transformers;

use App\Model\Arcliente;
use League\Fractal\TransformerAbstract;

class ArclienteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Arcliente $arcliente)
    {
        return [
            'name' => (string) $arcliente->NOMBRE,
            'lastName' => (string) $arcliente->APELLIDO,
            'businessName' => (string) $arcliente->RAZONS,
            'identification' => (string) $arcliente->CEDULA,
            'rucId' => (string) $arcliente->RUC,
            'address' => (string) $arcliente->DOMICILIO,
            'job_address' => (string) $arcliente->CL_DIRTRABAJO,
            'city' => (string) $arcliente->CIUDAD,
            'phone' => (string) $arcliente->TELEFONOS,
            'email' => (string) $arcliente->MAIL,
            'job_email' => (string) $arcliente->CL_MAILTRABAJO,
            'customerType' => (string) $arcliente->TIPOCLTE,
            'isActive' => ($arcliente->ESTADO === 'Activo'),
            'dcto' => (double) $arcliente->DESCUENTO,
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'name' => 'NOMBRE',
            'lastName' => 'APELLIDO',
            'businessName' => 'RAZONS',
            'identification' => 'CEDULA',
            'rucId' => 'RUC',
            'address' => 'DOMICILIO',
            'job_address' => 'CL_DIRTRABAJO',
            'city' => 'CIUDAD',
            'phone' => 'TELEFONOS',
            'email' => 'MAIL',
            'job_email' => 'CL_MAILTRABAJO',
            'customerType' => 'TIPOCLTE',
            'isActive' => 'ESTADO',
            'dcto' => 'DESCUENTO',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

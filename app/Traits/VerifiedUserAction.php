<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 13/02/2019
 * Time: 10:34
 */

namespace App\Traits;


trait VerifiedUserAction
{
    public function before($user, $ability) {
        if ($user->esVerificado()) {
            return true;
        }
    }

}
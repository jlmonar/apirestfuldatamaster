<?php

namespace App\Policies;

use App\Traits\VerifiedUserAction;
use App\User;
use App\Model\Dataset;
use Illuminate\Auth\Access\HandlesAuthorization;

class DatasetPolicy
{
    use HandlesAuthorization, VerifiedUserAction;

    /**
     * Determina cuando un usuario puede obtener la data de un usuario.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function getDatauser(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the dataset.
     *
     * @param  \App\User  $user
     * @param  \App\Model\Dataset  $dataset
     * @return mixed
     */
    public function view(User $user, Dataset $dataset)
    {
        //
    }

    /**
     * Determine whether the user can create datasets.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }
}
